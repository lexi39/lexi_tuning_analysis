%% Plotting the Average PSTH and Tuning Curve for Multiple Animals

%% Load All the Animal Data

%load and select the runStimLED file for all animals 
stackspath = uipickfiles('REFilter', '\.mat?$|\.bin$');
allanimal_path = fullfile(stackspath);

%makes blank cells to add the data in later
n_animals = size(allanimal_path,2);
prefer_res_noLED_allanimals = cell(1, n_animals);
prefer_res_LED_allanimals = cell(1, n_animals);
vis_cells_allanimals = cell(1, n_animals);
tuning_noLED_allanimals = cell(1, n_animals);
tuning_LED_allanimals = cell(1, n_animals);

%now actually adding all the data in 
%change depending on what loading
for ii = 1:n_animals 
    allanimal_data = load(allanimal_path{ii},'Prefer_res_noLED',...
                                       'Prefer_res_LED', 'nonvip_cells',...
                                       'tuning_matrix_noLED', 'tuning_matrix_LED');
    prefer_res_noLED_allanimals{ii} = allanimal_data.Prefer_res_noLED;
    prefer_res_LED_allanimals{ii} = allanimal_data.Prefer_res_LED;
    vis_cells_allanimals{ii} = allanimal_data.nonvip_cells; %change
    tuning_noLED_allanimals{ii} = allanimal_data.tuning_matrix_noLED;
    tuning_LED_allanimals{ii} = allanimal_data.tuning_matrix_LED;
    
end


%% Combine all the Data 

total_prefer_res_noLED = [];
total_prefer_res_LED = [];
total_tuning_noLED = [];
total_tuning_LED = [];
n_vis_cell = [];

for n = 1:n_animals
    
    prefer_noLED = prefer_res_noLED_allanimals{n};
    total_prefer_res_noLED = [total_prefer_res_noLED prefer_noLED];
    
    prefer_LED = prefer_res_LED_allanimals{n};
    total_prefer_res_LED = [total_prefer_res_LED prefer_LED];
    
    tune_noLED = tuning_noLED_allanimals{n};
    total_tuning_noLED = [total_tuning_noLED; tune_noLED];
    
    tune_LED = tuning_LED_allanimals{n};
    total_tuning_LED = [total_tuning_LED; tune_LED];
    
    n_cell = size(vis_cells_allanimals{n}, 1);
    n_vis_cell = [n_vis_cell n_cell];
    
end 

%% Generate Plots for All Animals

%define parameters for plots and data
win.vis = 24:35;
win.baseline_vis = 18:20;
win.LED = win.vis;
frameRate = 7.5;

hFig = figure('DefaultAxesFontSize',18);  
hFig.Units = 'normalized';
hFig.Position = [0.4 0.1 0.4 0.3];

lim1 = -.5;
lim2 = 5;
Lim3 = -.2;
Lim4 = 3;
Lim5 = 0.5;
Lim6 = 0.35;
Lim7 = 0;
Lim8 = 1;

%plot each cell 
subplot(1,3,1);hold on; 
plot(mean(total_prefer_res_noLED([win.LED],:))', mean(total_prefer_res_LED([win.LED],:))','o','color','b');   

xlim([Lim3 Lim4]);ylim([Lim3 Lim4]);
x = [lim1 lim2];y = [lim1 lim2];
line(x,y,'color',[0.8, 0.8,  0.8, 0.8]);
plot([lim1 lim2], [0 0], 'k');
plot([0 0], [lim1 lim2], 'k');
xlabel('dF/F (Laser Off)');ylabel('dF/F (Laser On)');
axis square;

%plot average psth
subplot(1,3,2);hold on;

shadedErrorBar([],mean(total_prefer_res_noLED,2),std(total_prefer_res_noLED,[],2)/sqrt(size(total_prefer_res_noLED,2)),'lineprops','k');
shadedErrorBar([],mean(total_prefer_res_LED,2),std(total_prefer_res_LED,[],2)/sqrt(size(total_prefer_res_LED,2)),'lineprops','m');

ylim([Lim3 Lim4]); xlim([0 60]);
xLED_win = frameRate*2 + 20;
xLED = [20 xLED_win xLED_win 20];yLED = [Lim3 Lim3 Lim4 Lim4];
patch(xLED,yLED,'k','EdgeColor','none','FaceAlpha',.05);
ylabel('dF/F');xlabel({'Time from'; 'Stimulus Onset (s)'});
set(gca,'XTick', [(20 - frameRate*2) 20 (frameRate*2 + 20), (frameRate*4 + 20)], 'XTickLabels', [-2 0 2 4]);
axis square;

%plot average tuning curve 
x1 = [1:8];
x2 = [1:8];

sem1 = std(total_tuning_noLED) / sqrt(length(total_tuning_noLED));
sem2 = std(total_tuning_LED) / sqrt(length(total_tuning_LED));

subplot(1,3,3);hold on;
errorbar(x1, mean(total_tuning_noLED), sem1, '-o', 'color', 'k','MarkerFaceColor','k', 'LineWidth', 1.5, 'Capsize', 0);
errorbar(x2, mean(total_tuning_LED), sem2, '-o', 'color', 'm','MarkerFaceColor','m', 'LineWidth', 1.5, 'Capsize', 0);

xticks([1:8]);
xticklabels({'-90', '-45' ,'0', '45', '90' ,'135', '180', '225'});
xtickangle(45)
ylim([Lim3 Lim4]);
xlabel('Direction (^{o})');
ylabel('dF/F');
legend ('LED OFF', 'LED ON');
axis square;

sgt = sgtitle(sprintf('Laser Effect on Visual Cells (n=%G)', sum(n_vis_cell))); %change
sgt.FontSize = 18;
 