function [snippets, timePSTH] = extract_snippets(rawData, vnStimSamples, ...
    vWindow, vVerbose)
%EXTRACT_SNIPPETS Extract snippets around all vnStimSamples
% Inputs:
% - rawData: a N rois by N frames matrix with raw data
% - vnStimSamples: samples to trigger snippet extraction
% - vWindow: window for the PSTH (in samples)
% - vVerbose: print warning when ignoring triggers close to borders of raw
%       data

if ~exist('vVerbose', 'var') || isempty(vVerbose)
    vVerbose=1;
end
    
% create an index around vWindow
timePSTH = (0:diff(vWindow)) + vWindow(1);

% make an empty psth output
nSamples = numel(vnStimSamples);
snippet_shape = [numel(timePSTH), size(rawData, 1), nSamples];
total_size = prod(snippet_shape) * 8 / 2^30;  % size in Gbytes
    
if vVerbose  && (total_size > 1)
    
    fprintf('Gonna create a big matrix. Roughly %0.2i gb.\n', total_size)
    answer = input('Is that fine? (''Y''/''n'')', 's');
    
    while ~ismember(lower(answer), {'y', 'n', ''})
        answer = input('''y'' for yes, ''n'' for no');
    end
    
    if strcmpi(answer, 'n')
        snippets = 0;
        timePSTH = 0;
        return
    end
end
  
%%%%%%%
goodTrigs = vnStimSamples((vnStimSamples + vWindow(1) >= 1) & ...
    vnStimSamples + vWindow(2) <= size(rawData, 2));

if vVerbose > 0
    if numel(goodTrigs) ~= numel(vnStimSamples)
        fprintf('%i trigger(s) too close from borders.\n', ...
            numel(vnStimSamples)-numel(goodTrigs))
    end
end

snippets = zeros(snippet_shape);

for iTrig = 1:numel(goodTrigs)
    trig = goodTrigs(iTrig);
end

%%%%%%%%
vbGoodTrigsMask = (vnStimSamples + vWindow(1) >= 1) & ...
    vnStimSamples + vWindow(2) <= size(rawData, 2);

if vVerbose > 0
    nGoodTrigs = sum(vbGoodTrigsMask);
    if nGoodTrigs ~= nSamples
        fprintf('%i trigger(s) too close from borders.\n', ...
            nSamples - nGoodTrigs)
    end
end

snippets = nan(snippet_shape);

for iTrig = 1:nSamples
    if ~vbGoodTrigsMask(iTrig)
        continue;
    end
    trig = vnStimSamples(iTrig);
    snippet = double(rawData(:, trig + vWindow(1): trig + vWindow(end)));
    snippets(:, :, iTrig) = snippet';
end