function [tableStimulation] = makeStimulationTable(sParams)
%MAKESTIMULATIONTABLE Create a stimulation table for experiments with LED/no LED + vis stim
% Inputs:
% - sParams: the structure saved in the mat file by sftf protocol with led
%
% Outputs:
% -tableStimulation: a table with stimulation information, namely SF, TF,
% direction, led (0 for no, 1 for yes), trialType (0 for vis only, 1 for
% vis + led, 2 for led only), timestamps (software time from matlab)

sStimulation = struct();
sStimulation.StimId = sParams.orderVec';
sStimulation.index = (1:numel(sParams.orderVec))';
sStimulation.timestamps = sParams.timestamps;

% Create trialType that will be 0 for vis only, 1 for vis + led, 2 for led
% only
trialType = nan(size(sParams.orderVec));

% Find trials with LED only
% Led only have the last stim IDs
ledOnlyStimId = (1:sParams.nLedOnly) + numel(sParams.stimulationMat);
% Now find them in orderVec
ledOnlyTrial = ismember(sParams.orderVec, ledOnlyStimId);
trialType(ledOnlyTrial) = 2;

% Get stimulation information for trials with visual stimulation
visStimInfo = struct();
visOrderVec = sParams.orderVec(~ledOnlyTrial);
direction = sParams.directionMat(:)';
visStimInfo.direction = sParams.gratingDirection(direction(visOrderVec));
sf = sParams.spatialFrequencyMat(:)';
visStimInfo.sf = sParams.spatialFrequency(sf(visOrderVec));
tf = sParams.temporalFrequencyMat(:)';
visStimInfo.tf = sParams.temporalFrequency(tf(visOrderVec));
led = sParams.stimulationMat(:)';
visStimInfo.led = led(visOrderVec);

% Now populate structure avoid led only trials
csFieldNames = fieldnames(visStimInfo);
for iF = 1:numel(csFieldNames)
    sField = csFieldNames{iF};
    vec = nan(size(sParams.orderVec));
    vec(~ledOnlyTrial) = visStimInfo.(sField);
    sStimulation.(sField) = vec';
end
% For trial type, I put led in the non-led only fields (that are already
% set to 2)
trialType(~ledOnlyTrial) = visStimInfo.led;
sStimulation.trialType = trialType';
% For led I put 1 for led only trials
sStimulation.led(ledOnlyTrial) = 1;

% Make a table
tableStimulation = struct2table(sStimulation);