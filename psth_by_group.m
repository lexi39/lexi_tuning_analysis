function [psth, groupNames, timePSTH] = psth_by_group(rawData, tableStimulation, ...
    csValues2Group, sCol2Trig, vWindow, vVerbose)
%PSTH_BY_GROUP Make a PSTH for each combination of csValues2Group
% Inputs:
% - rawData: a N rois by N frames matrix with raw data
% - tableStimulation: a table with stimulation information (as returned by
% makeStimulationTable) and with trigger information
% - csValues2Group: a cell of column names on which you want to group the
%       data. All the lines with a NaN for any of the value will be ignored
% - sCol2Trig: the name of the column with trigger sample information
% - vWindow: window for the PSTH (in samples)
% - vVerbose: print warning when ignoring triggers close to borders of raw
%       data


if ~exist('vVerbose', 'var') || isempty(vVerbose)
    vVerbose=1;
end

[groupID, groupNames] = findgroups(tableStimulation(:,csValues2Group));

% create an index around vWindow
timePSTH = (0:diff(vWindow)) + vWindow(1);

% make an empty psth output
psth = zeros([numel(timePSTH), size(rawData, 1), size(groupNames, 1)]);

for iGp = 1:size(groupNames, 1)
    % get the triggers for that group
    trigs = tableStimulation{groupID == iGp, sCol2Trig};
    goodTrigs = trigs((trigs + vWindow(1) >= 1) & ...        
            trigs + vWindow(2) <= size(rawData, 2));
    if vVerbose > 0
        if numel(goodTrigs) ~= numel(trigs)
            fprintf('%i trigger(s) too close from borders for group %i.\n', ...
                    numel(trigs)-numel(goodTrigs), iGp)
        end
    end
    for iTrig = 1:numel(goodTrigs)
        trig = goodTrigs(iTrig);
        snippet = rawData(:, trig + vWindow(1): trig + vWindow(end));
        psth(:, :, iGp) = psth(:, :, iGp) + snippet' / numel(goodTrigs);
    end
end

end