%% Plotting the Average PSTH and Tuning Curve for Non-Annotated ROIs

%% Load preprocessed data and corresponding stimulation file

% load sParams from the mat file, dff and simetadata
% need to load the dff file from processed_extract.mat
% load entire processed_2passes_metadata.mat file
% load sParams file from LEDStim.mat

%default is 1, 
%when combine ROI analysis, part 1 is stack=1 and part 2 is stack=2
stack = 1; 

% Get stimulation table

triggers1_onset = reshape(~cellfun(@isempty,{si_metada{1}.auxTrigger1}), size(si_metada{1}));
tableStimulation = makeStimulationTable(sParams);
triggers = sort(vertcat(find(triggers1_onset(1,1,:)),find(triggers1_onset(2,1,:)),...
    find(triggers1_onset(3,1,:)),find(triggers1_onset(4,1,:))));

% check that I have 1 and only 1 trigger per stim. Otherwise, crash
assert(size(tableStimulation, 1) == numel(triggers))

% add the triggers to the stimulation table
tableStimulation.('trial_trigger') = triggers;
if exist('triggers0_onset', 'var')
    triggers0 = find(triggers0_onset);
    led_trigger = nan([size(tableStimulation, 1), 1]);
    led_trigger(boolean(tableStimulation.led)) = triggers0;
    tableStimulation.('led_trigger') = led_trigger;
end

nDir=8;
nLed = 2;
nrois = size(dff,2);

for x = 1:length(dff)
    
    rawData = zeros(nrois, size(dff(stack,x).activity, 2));
    
    if isempty(rawData)
        continue
        
    else 
        break
    end
    
end

for niRoi = 1:nrois
    
    if isempty(dff(stack,niRoi).activity)
        continue
    end
    
    rawData(niRoi,:) = dff(stack,niRoi).activity;
end

% Eliminate ROIs with no activity 
%rawData = rawData(any(rawData,2),:);
%nrois = size(rawData,1);

% Extract snippets and make a PSTH for every direction

frameRate = 7.5;
sCol2Trig = 'trial_trigger';
vWindow = [-19,40];
% vWindow = [-4,6] * frameRate;
vVerbose = 1;
csValues2Group = {'direction','trialType'};

[groupID, ~] = findgroups(tableStimulation(:,csValues2Group));
[snippets, ~] = extract_snippets(rawData, triggers, vWindow, vVerbose);
[psth, groupNames, timePSTH] = psth_by_group(rawData, tableStimulation, ...
    csValues2Group, sCol2Trig, vWindow, vVerbose);

%loads all the needed frames for the rest of the code
win.vis = 24:35; %changed from 40 so ends at 2s after
win.baseline_vis = 18:20;
win.LED = win.vis;

%% pick up non-annotated ROIs

%calculating the mean response for all the ROIs 
for n = 1:nrois
    mean_response(n,:) = squeeze(mean(psth([win.vis],n,:),1)) - squeeze(mean(psth([win.baseline_vis],n,:),1));
end
[Max(:,1),Index(:,1)] = max(mean_response(:,(1:2:end)),[],2);


%select all the non-annotated ROIs (can add visual threshold by removing
%comments)

%threshold.vis = 0.3
nonvipI = false(nrois,1);
for ii=1:nrois
    
    if isempty(dff(1,ii).annotations) == true %&& Max(ii,1) >= threshold.vis
        nonvipI(ii,1)= true;
    end
   
end

%select activity based on all directions
nonvip_cells = find(nonvipI);
for d = 1:nDir
    for ii=1:numel(nonvip_cells)
        psth_noLED(:,ii,d) = psth(:, nonvip_cells(ii), (2*d-1));
        psth_LED(:,ii,d) = psth(:, nonvip_cells(ii), (2*d));
    end
end

Prefer_res_noLED = squeeze(mean(psth_noLED, 3));
Prefer_res_LED = squeeze(mean(psth_LED, 3));

%select activity based on preferred direction
%nonvip_cells = find(nonvipI);
%Prefer_index = Index(nonvip_cells);

%for i = 1:numel(nonvip_cells)
%    p_index = 2*(Prefer_index(i) - 1)+1;
%    Prefer_res_noLED(:,i) = squeeze(psth(:,nonvip_cells(i), p_index))';
%    Prefer_res_LED(:,i) = squeeze(psth(:,nonvip_cells(i), p_index +1))';
%end

%% %% Generate Plots for All Marked Cells 

%set parameters for first plot
hFig = figure('DefaultAxesFontSize',18);  
hFig.Units = 'normalized';
hFig.Position = [0.4 0.1 0.4 0.3];

lim1 = -.5;
lim2 = 5;
Lim3 = -0.2;
Lim4 = 3; 
Lim5 = 1;
Lim6 = 0.35;
Lim7 = 0;

%plot each cell 
subplot(1,3,1);hold on; 
plot(mean(Prefer_res_noLED([win.LED],:))', mean(Prefer_res_LED([win.LED],:))','o','color','b');   

%xlim([lim1 lim2]);ylim([lim1 lim2]);
xlim([Lim3 Lim4]);ylim([Lim3 Lim4]);
x = [lim1 lim2];y = [lim1 lim2];
line(x,y,'color',[0.8, 0.8,  0.8, 0.8]);
plot([lim1 lim2], [0 0], 'k');
plot([0 0], [lim1 lim2], 'k');
xlabel('dF/F (Laser Off)');ylabel('dF/F (Laser On)');
axis square;

%plot average PSTH
subplot(1,3,2);hold on;
shadedErrorBar([],mean(Prefer_res_noLED,2),std(Prefer_res_noLED,[],2)/sqrt(size(Prefer_res_noLED,2)),'lineprops','k');
shadedErrorBar([],mean(Prefer_res_LED,2),std(Prefer_res_LED,[],2)/sqrt(size(Prefer_res_LED,2)),'lineprops','m');

%ylim([-.05 0.2])
%ylim([Lim3 Lim4])
%[Lim7 Lim6]
ylim([Lim3 Lim4]); xlim([0 60]);
xLED_win = frameRate*2 + 20;
xLED = [20 xLED_win xLED_win 20];yLED = [Lim3 Lim3 Lim4 Lim4];
patch(xLED,yLED,'k','EdgeColor','none','FaceAlpha',.05);
ylabel('dF/F');xlabel({'Time from'; 'Stimulus Onset (s)'});
set(gca,'XTick', [(20 - frameRate*2) 20 (frameRate*2 + 20), (frameRate*4 + 20)], 'XTickLabels', [-2 0 2 4]);
axis square;

% plot average tuning curve 

tuning_matrix_noLED = []; 
tuning_matrix_LED = [];


for iRoi = find(nonvipI)' 
   
    x1 = [];
    y1 = [];
    x2 = [];
    y2 = [];

    for iDir = 1:8
       
        for iLed = 1:nLed

            currentID = nLed * (iDir - 1) + iLed;
            act = mean(psth([win.LED], iRoi, currentID));
            
                if iLed == 1 %if 1, then no LED  
                x1 = [x1, iDir];
                y1 = [y1, act];
                else %if 2 (anything else besides 1) then LED
                x2 = [x2, iDir];
                y2 = [y2, act];
                end        
        end
        
        %for tuning curve based on averaged activity:
        y= y1 + y2;
        y_avg = y/2;
        index_max = find(y_avg==max(y_avg));
        
        %find the index of the max act of non LED
        %index_max = find(y1==max(y1));
        
        %calc diff between index_max and 90 (3 index)
        shift_index = 3- index_max;
        y1_shift = circshift(y1, shift_index);
        y2_shift = circshift(y2, shift_index);
        
    end 
           
    tuning_matrix_noLED = [tuning_matrix_noLED; y1_shift];
    tuning_matrix_LED = [tuning_matrix_LED; y2_shift];
    
end

%calculate error
sem1 = std(tuning_matrix_noLED) / sqrt(length(tuning_matrix_noLED));
sem2 = std(tuning_matrix_LED) / sqrt(length(tuning_matrix_LED));

%plot average tuning curve
subplot(1,3,3);hold on;
errorbar(x1, mean(tuning_matrix_noLED), sem1, '-o', 'color', 'k','MarkerFaceColor','k', 'LineWidth', 1.5, 'Capsize', 0);
errorbar(x2, mean(tuning_matrix_LED), sem2, '-o', 'color', 'm','MarkerFaceColor','m', 'LineWidth', 1.5, 'Capsize', 0);

xticks([1:8]);
xticklabels({'-90', '-45' ,'0', '45', '90' ,'135', '180', '225'});
xtickangle(45)
%ylim([0 3]);
%[Lim7 Lim6]
ylim([Lim3 Lim4]);
xlabel('Direction (^{o})');
ylabel('dF/F');
legend ('LED OFF', 'LED ON');
axis square;

n_vip_cell = size(Prefer_res_LED,2);
sgt = sgtitle(sprintf('Laser Effect on Non-Marked Cells (n=%G)', n_vip_cell));
sgt.FontSize = 18;
